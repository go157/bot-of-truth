// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package messagechecker_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/messagechecker"
)

type testWord struct {
	word    string
	isMatch bool
}

func TestGetReqexp(t *testing.T) {
	t.Parallel()

	wordsToTest := []testWord{
		{"SВЕR", true},
		{"SВЕЯ", true},
		{"SВER", true},
		{"$ВER", true},
		{"Sber", true},
		{"Sber", true},
		{"Sber", true},
		{"Сбер", true},
		{"Sbеr", true},
		{"Sbеr", true},
		{"Sber", true},
		{"SВER", true},
		{"SBER", true},
		{"Sbеr", true},
		{"Cбep", true},
		{"Cбeр", true},
		{"Cбеp", true},
		{"Cбер", true},
		{"Cбеp", true},
		{"sber", true},
		{"$бер", true},
		{"Sьеr", true},
		{"Cбербанк", true},
		{"cбербанк", true},
		{"S b e r", true},
		{"Cбербанк", true},
		{"Cбережения", false},
		{"с    бе р", true},
		{"зеленый банк", true},
		{"зеленая контора", true},
		{"Работает в поддержке зеленой конторы))", true},
		{"зеленый стул", false},
		{"зеленый слон", false},
		{"Гибнут стада, родня погибает", false},
		{"И смертен ты сам, но знаю одно", false},
		{"Смерти не знает добрая слава деяний достойных", false},
		{"С6е₽", true},
		{"Сб●р", true},
		{"С■ер", true},
		{"***р", true},
		{" ***** ", false},
		{"Кстати.Ты когда в $βΣ₱* устроишься покажешь им бота?", true},
		{"С-ер", true},
		{"Сб@р", true},
		{"Сбёр", true},
		{"Сбəp", true},
		{"С/бер/", true},
		{"©бер®", true},
		{"Постоянно слышу что сбр гавно.Но в основном это говорят люди из убыточных шараг))", true},
		{"сбербанкушка", true},
		{"сбербанксбербанк", true},
		{"сберсберсбер", true},
		{"СБЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕр", true},
		{"СССССССБЕР", true},
		{"ССБЕР", true},
		{"СБЕЕР", true},
		{"СББЕР", true},
		{"СБЕРР", true},
		{"с̱бeр", true},
		{"cбèр", true},
	}

	sberChecker := messagechecker.GetCheker("((зелен(ый|ого|ому|ым|ом|ая|ой|ую|ой)(банк|контор))|(сбр)|(сбер(бнк|банк)?))(|а|у|ом|е|ы|ой|ушка)", true)
	for _, test := range wordsToTest {
		t.Run(fmt.Sprintf("matcher for %s", test.word), func(t *testing.T) {
			require.Equal(t, sberChecker.CheckMessage(test.word), test.isMatch)
		})
	}
}
