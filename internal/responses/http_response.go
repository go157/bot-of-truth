package responses

import (
	"bytes"
	"context"
	"encoding/json"
	"text/template"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/requester"
)

type httpResponse struct {
	content    string
	url        string
	method     string
	message    string
	selfTarget bool
}

// HTTPResponseDescription - response desc
type HTTPResponseDescription struct {
	URL     string
	Method  string
	Body    string
	Message string
}

type templateDescription struct {
	HTTPResponse  map[string]interface{}
	TargetMessage *models.Message
}

// NewHTTPResponse - response by send http request
func NewHTTPResponse(desc HTTPResponseDescription, reply bool) Response {
	return &httpResponse{
		content:    desc.Body,
		url:        desc.URL,
		method:     desc.Method,
		message:    desc.Message,
		selfTarget: reply,
	}
}

func (response *httpResponse) React(ctx context.Context, client Client, targetMessage *models.Message) error {
	request := requester.Request{
		InputMessage: targetMessage.Text,
		Body:         response.content,
		URL:          response.url,
		Method:       response.method,
	}
	body, err := requester.RequestByTemplate(ctx, request)
	if err != nil {
		return err
	}

	if response.message == "" {
		return nil
	}
	var bodyMap map[string]interface{}
	err = json.Unmarshal(body, &bodyMap)
	if err != nil {
		return err
	}

	messageParams := templateDescription{
		HTTPResponse:  bodyMap,
		TargetMessage: targetMessage,
	}
	messageText, err := formatMessage(response.message, messageParams)
	if err != nil {
		return err
	}

	message := &bot.SendMessageParams{
		ChatID: targetMessage.Chat.ID,
		Text:   messageText,
	}
	if response.selfTarget {
		message.ReplyToMessageID = targetMessage.ID
	}
	_, err = client.SendMessage(
		ctx,
		message,
	)

	return err
}

func formatMessage(rawMessage string, messageParams templateDescription) (string, error) {
	bodyTamplete, err := template.New("body").Parse(rawMessage)
	if err != nil {
		logger.LogErr(err.Error())

		return "", err
	}

	templateWriter := new(bytes.Buffer)
	err = bodyTamplete.Execute(templateWriter, messageParams)

	if err != nil {
		logger.LogErr(err.Error())

		return "", err
	}

	return templateWriter.String(), nil
}
