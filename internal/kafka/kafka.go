package kafka

import (
	"context"
	"errors"
	"fmt"
	"time"

	kafka_client "github.com/segmentio/kafka-go"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
)

var ErrIncorrectConfig = errors.New("incorrect config")

type Consumer interface {
	Run()
}

// Handler - handle input messages
type Handler interface {
	Handle(context.Context, kafka_client.Message) error
}

type consumer struct {
	client   *kafka_client.Reader
	handler  Handler
	settings Settings
}

type Settings struct {
	Brokers      []string
	Topic        string
	GroupID      string
	Timeout      time.Duration
	RetryTimeout time.Duration
	RetryCount   int8
}

func NewConsumer(
	settings Settings,
	handler Handler,
) (Consumer, error) {
	err := validateSettings(settings)
	if err != nil {
		return nil, err
	}

	client := kafka_client.NewReader(kafka_client.ReaderConfig{
		Brokers: settings.Brokers,
		Topic:   settings.Topic,
		GroupID: settings.GroupID,
	})

	return &consumer{
		client:   client,
		settings: settings,
		handler:  handler,
	}, nil
}

func validateSettings(settings Settings) error {
	switch {
	case len(settings.Brokers) == 0:
		return fmt.Errorf("%w: has no brokers", ErrIncorrectConfig)
	case settings.Topic == "":
		return fmt.Errorf("%w: has no topic", ErrIncorrectConfig)
	case settings.GroupID == "":
		return fmt.Errorf("%w: has no group id", ErrIncorrectConfig)
	}

	return nil
}

func (c *consumer) Run() {
	go c.RunInBG()
}

func (c *consumer) RunInBG() {
	for {
		c.consume()
	}
}

func (c *consumer) consume() {
	ctx := context.Background()

	msg, err := c.client.ReadMessage(ctx)
	if err != nil {
		logger.LogErr(fmt.Sprintf("kafka: cant read %s: %s", string(msg.Value), err))

		return
	}

	if c.settings.Timeout.Nanoseconds() > 0 {
		ctxTimeout, cancel := context.WithTimeout(ctx, c.settings.Timeout)
		defer cancel()
		ctx = ctxTimeout
	}
	err = c.handle(ctx, msg)
	if err != nil {
		for i := 0; i < int(c.settings.RetryCount); i++ {
			time.Sleep(c.settings.RetryTimeout)
			err = c.handle(ctx, msg)
			if err == nil {
				break
			}
		}
	}

	if err != nil {
		logger.LogErr(fmt.Sprintf("kafka: cant handle %s: %s", string(msg.Value), err))
	}
}

func (c *consumer) handle(ctx context.Context, msg kafka_client.Message) error {
	if c.settings.Timeout.Nanoseconds() > 0 {
		ctxTimeout, cancel := context.WithTimeout(ctx, c.settings.Timeout)
		defer cancel()
		ctx = ctxTimeout
	}

	return c.handler.Handle(ctx, msg)
}
