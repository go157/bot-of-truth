package messageshandler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/go-telegram/bot"
	kafka_client "github.com/segmentio/kafka-go"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/interfaces"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/kafka"
)

var ErrWrongMessage = errors.New("wrong message")

type Message struct {
	Text   string `json:"text"`
	ChatID int64  `json:"chat_id"`
}

type handler struct {
	client interfaces.MessageSender
}

// NewHandler - Handler constructor
func NewHandler(client interfaces.MessageSender) kafka.Handler {
	return &handler{
		client: client,
	}
}

func (h *handler) Handle(ctx context.Context, rawMsg kafka_client.Message) error {
	var msg Message
	err := json.Unmarshal(rawMsg.Value, &msg)
	if err != nil {
		return err
	}

	if msg.ChatID == 0 || msg.Text == "" {
		return fmt.Errorf("%w: message: %s", ErrWrongMessage, string(rawMsg.Value))
	}

	_, err = h.client.SendMessage(
		ctx,
		&bot.SendMessageParams{
			ChatID: msg.ChatID,
			Text:   msg.Text,
		},
	)

	return err
}
