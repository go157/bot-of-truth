package mocks

import (
	"time"

	"github.com/go-telegram/bot/models"
	"syreclabs.com/go/faker"
)

func RandMessage() *models.Message {
	return &models.Message{
		ID:              faker.RandomInt(10000, 20000),
		MessageThreadID: faker.RandomInt(10000, 20000),
		From: &models.User{
			ID:           faker.RandomInt64(10000, 20000),
			IsBot:        false,
			FirstName:    faker.Name().FirstName(),
			LastName:     faker.Name().LastName(),
			Username:     faker.Internet().UserName(),
			LanguageCode: "ru",
			IsPremium:    false,
		},
		Date: int(time.Now().Unix()),
		Chat: models.Chat{
			ID:          faker.RandomInt64(10000, 20000),
			Type:        "supergroup",
			Title:       "some chant",
			Username:    "some_chat",
			IsForum:     true,
			Description: faker.Lorem().Sentence(10),
		},
		Text: faker.Lorem().Sentence(50),
	}
}
