package requester

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"strings"
	"text/template"
	"time"

	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
)

// Request - descriptions how to sent request.
type Request struct {
	Body         string
	InputMessage string
	URL          string
	Method       string
}

type templateFormat struct {
	name     string
	template string
	params   TemplateParams
}

// TemplateParams - has parametrs to add to template.
type TemplateParams struct {
	Msg []string
}

// RequestByTemplate - send request with adding parametrs from message.
func RequestByTemplate(ctx context.Context, templateRequest Request) ([]byte, error) {
	templateParams := TemplateParams{
		Msg: strings.Split(templateRequest.InputMessage, " "),
	}

	bodyFormat := templateFormat{
		name:     "body",
		params:   templateParams,
		template: templateRequest.Body,
	}
	body, err := format(bodyFormat)
	if err != nil {
		return nil, err
	}

	urlFormat := templateFormat{
		name:     "url",
		params:   templateParams,
		template: templateRequest.URL,
	}
	url, err := format(urlFormat)
	if err != nil {
		return nil, err
	}

	bodyReader := strings.NewReader(body)
	timeoutCtx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()
	req, err := http.NewRequestWithContext(timeoutCtx, templateRequest.Method, url, bodyReader)
	if err != nil {
		return nil, err
	}

	hc := http.Client{}
	resp, err := hc.Do(req)
	if err != nil {
		return nil, err
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	resp.Body.Close()

	return respBody, nil
}

func format(format templateFormat) (string, error) {
	bodyTamplete, err := template.New("body").Parse(format.template)
	if err != nil {
		logger.LogErr(err.Error())

		return "", err
	}

	templateWriter := new(bytes.Buffer)
	err = bodyTamplete.Execute(templateWriter, format.params)

	if err != nil {
		logger.LogErr(err.Error())

		return "", err
	}

	return templateWriter.String(), nil
}
