// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package performance

import (
	"fmt"
	"time"

	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
)

// Measuring - object to watch performance.
type Measuring struct {
	start   int64
	end     int64
	message string
}

// Start - returt new performance check.
func Start() Measuring {
	newMeasuring := Measuring{
		start: time.Now().UnixMilli(),
	}

	return newMeasuring
}

// End - stop performance check and print result.
func (measuring Measuring) End(message string) {
	measuring.end = time.Now().UnixMilli()
	measuring.message = message
	measuring.writeToLog()
}

func (measuring Measuring) writeToLog() {
	duration := measuring.end - measuring.start
	messageToLog := fmt.Sprintf("%d: %s", duration, measuring.message)
	logger.LogPerformance(messageToLog)
}
