// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"gitlab.com/ffSaschaGff/bot-of-truth/internal/bot"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/botsettings"
	telegramclient "gitlab.com/ffSaschaGff/bot-of-truth/internal/client/telegram_client"
	hookschecker "gitlab.com/ffSaschaGff/bot-of-truth/internal/hooks_checker"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/interfaces"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/kafka"
	messageshandler "gitlab.com/ffSaschaGff/bot-of-truth/internal/kafka/messages_handler"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	// Read settings, init bot
	settings := botsettings.GetSettings()
	app := bot.NewBot(settings)
	tclient := telegramclient.New(ctx, app.Handle)

	messagesHandler := messageshandler.NewHandler(tclient.GetAPI())

	// Start kafka
	kafkaClient, errKafka := kafka.NewConsumer(
		kafka.Settings{
			Brokers:      settings.Kafka.Host,
			GroupID:      settings.Kafka.ConsumerGroup,
			Topic:        settings.Kafka.MessagesTopic,
			Timeout:      time.Millisecond * time.Duration(settings.Kafka.Timeout),
			RetryTimeout: time.Millisecond * time.Duration(settings.Kafka.RetryTimeout),
			RetryCount:   settings.Kafka.RetryCount,
		},
		messagesHandler,
	)
	if errKafka != nil {
		logger.LogErr(errKafka.Error())
	} else {
		kafkaClient.Run()
	}

	// Start hooks watching
	go watchHooks(ctx, settings.Hooks, tclient.GetAPI())

	// Go! Go! Go!!111
	tclient.Run(ctx)
}

func watchHooks(
	ctx context.Context,
	hooks []*hookschecker.Hook,
	api interfaces.MessageSender,
) {
	for _, hook := range hooks {
		hook.Watch(ctx, api)
	}
}
