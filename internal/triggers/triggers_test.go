package triggers

import (
	"testing"

	"github.com/go-telegram/bot/models"
	"github.com/stretchr/testify/require"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/mocks"
)

func Test_chatTrigger_Check(t *testing.T) {
	t.Parallel()

	type fields struct {
		chats []int64
	}
	tests := []struct {
		name          string
		fields        fields
		targetMessage *models.Message
		want          bool
	}{
		{
			name: "positive case",
			fields: fields{
				chats: []int64{100, 200, 300},
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				m.Chat.ID = 200
				return m
			}(),
			want: true,
		},
		{
			name: "negatice case",
			fields: fields{
				chats: []int64{100, 200, 300},
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				m.Chat.ID = 201
				return m
			}(),
			want: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			trigger := NewChatTrigger(tt.fields.chats)
			got := trigger.Check(tt.targetMessage)
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_regexpTrigger_Check(t *testing.T) {
	t.Parallel()

	type fields struct {
		regexp      string
		unavoidable bool
	}
	tests := []struct {
		name          string
		fields        fields
		targetMessage *models.Message
		want          bool
	}{
		{
			name: "positive case",
			fields: fields{
				regexp:      "some message",
				unavoidable: false,
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				m.Text = "I have some message"
				return m
			}(),
			want: true,
		},
		{
			name: "negative case",
			fields: fields{
				regexp:      "some text",
				unavoidable: false,
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				m.Text = "I dont have this message"
				return m
			}(),
			want: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			trigger := NewMessageRegexp(
				tt.fields.regexp,
				tt.fields.unavoidable,
			)
			got := trigger.Check(tt.targetMessage)
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_stickerTrigger_Check(t *testing.T) {
	t.Parallel()

	type fields struct {
		stickerID string
	}
	type args struct {
		targetMessage *models.Message
	}
	tests := []struct {
		name          string
		fields        fields
		targetMessage *models.Message
		want          bool
	}{
		{
			name: "positive case",
			fields: fields{
				stickerID: "qwerty123456",
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				m.Sticker = &models.Sticker{
					FileUniqueID: "qwerty123456",
				}
				return m
			}(),
			want: true,
		},
		{
			name: "negative case",
			fields: fields{
				stickerID: "qwerty123456",
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				m.Sticker = &models.Sticker{
					FileUniqueID: "pqwerty1234567",
				}
				return m
			}(),
			want: false,
		},
		{
			name: "negative case (has no stiker)",
			fields: fields{
				stickerID: "qwerty123456",
			},
			targetMessage: func() *models.Message {
				m := mocks.RandMessage()
				return m
			}(),
			want: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			trigger := NewStickerTrigger(tt.fields.stickerID)
			got := trigger.Check(tt.targetMessage)
			require.Equal(t, tt.want, got)
		})
	}
}
