// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package messagechecker

import (
	"regexp"
	"strings"

	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
)

// Checker - type to check if message is target.
type Checker struct {
	match  *regexp.Regexp
	filter *regexp.Regexp
}

var letterReplases = map[string]string{
	"а": "а|a|а|ạ|ą|ä|à|á|ą",
	"б": "β|В|б|в|b|6|ь|þ|ß|ƅ|Ƅ|Ƌ|β|ҕ|ᶀ|ḅ|ḇ",
	"в": "β|8|b|в|v",
	"г": "г|g",
	"д": "д|d",
	"е": "ə|Σ|Е|e|е|ё|е|ẹ|ė|é|è",
	"ё": "ə|Σ|Е|e|е|ё",
	"ж": "ж|j|g",
	"з": "3|з|z",
	"и": "й|и|n|i",
	"й": "й|и|n|i",
	"к": "к|k",
	"л": "l|л",
	"м": "m|м",
	"н": "н|n|h|һ",
	"о": "o|о|0|о|ο|օ|ȯ|ọ|ỏ|ơ|ó|ò|ö",
	"п": "₽|₱|п|p",
	"р": "₽|₱|р|p|r|я",
	"с": "С|s|с|\\$|c|с|ƈ|ċ",
	"т": "t|т",
	"у": "у|y|u",
	"ф": "f|ф",
	"х": "х|x|х|ҳ",
	"ц": "ц|c|с",
	"ч": "ch|4|ч",
	"ъ": "ь|ъ|b",
	"ы": "ы|ь\\||ъ\\||b\\|",
	"ь": "ь|ъ|b",
	"э": "э|e|е",
	"ю": "ю|j",
	"я": "я|r|ja",
}

// GetCheker - return checker by it description.
func GetCheker(inputString string, unavoidable bool) *Checker {
	checker := new(Checker)

	if unavoidable {
		checker.filter = regexp.MustCompile(`[^-_\*\s\.\?!,^$\)\(]`)
		checker.match = getReqexp(inputString)
	} else {
		checker.filter = nil
		checker.match, _ = regexp.Compile(inputString)
	}

	return checker
}

// CheckMessage - check message.
func (checker Checker) CheckMessage(message string) bool {
	matches := checker.match.FindAllString(message, -1)

	if len(matches) != 0 && checker.filter == nil {
		return true
	}

	for _, match := range matches {
		if checker.filter.MatchString(match) {
			return true
		}
	}

	return false
}

func getReqexp(inputString string) *regexp.Regexp {
	wordSeparators := "([\\x{02C6}-\\x{0362}]|[\\x{0591}-\\x{05C5}]|©|@|®|\\/|\\|-|_|\\*|\\s|\\.|\\?|!|,|^|$|\\)|\\()"
	regexpStr := "(?i)" + wordSeparators + "("
	prevLetter := "|"
	techSymbols := "(|)?"

	for letterPosition, rune := range inputString {
		letter := string(rune)
		if strings.Contains(techSymbols, letter) {
			regexpStr += letter

			continue
		}

		if letterPosition != 0 && !strings.Contains(techSymbols, prevLetter) && !strings.Contains(techSymbols, letter) {
			regexpStr += wordSeparators + "*"
		}

		if replace, exist := letterReplases[letter]; exist {
			regexpStr += "(©|@|-|®|\\/|\\\\|●|■|\\*|" + replace + ")+"
		} else {
			regexpStr += "(©|@|-|®|\\/|\\\\|●|■|\\*|" + letter + ")+"
		}

		prevLetter = letter
	}

	regexpStr += ")+" + wordSeparators

	logger.LogDebug("regexp for " + inputString + " is " + regexpStr)
	outputRegexp, err := regexp.Compile(regexpStr)
	if err != nil {
		logger.LogErr("Cant compile regexp for " + inputString)
	}

	return outputRegexp
}
